import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParseTest {
    
    public static void main( String[] args )
    {
        // Test different scenarios.
        JQLParser parser = new JQLParser();
        ParseResult result = null;

        parser.parseJql("1", "project = EFSCCC AND component = " +
            "\"EFSCCC:DFS - Collections - Team Seahawks\" ORDER BY Rank ASC");
        parser.parseJql("2", "issuetype in (Bug, \"Bug - Simple Workflow\"," +
            " Task) AND status in (new, open, Fixed) AND component in " +
            "(\"BSWMDBS2:Samson Wild Fire\", \"BSWMDBS:Samson Wild Fire\", " +
            "\"BSWMDBN2:Samson Wild Fire\", \"BSWMBMP:Samson Wild Fire\") AND " +
            "\"Detected During\" != Security ORDER BY severity ASC, priority DESC, created ASC");
        parser.parseJql("3", "project = TEQPM AND component in (\"TEQPM:Portfolio " +
            "Management\", \"TEQPM:TEQ WM & Tools Adoption\") ORDER BY Rank ASC");
        parser.parseJql("4", "project = TEQPM AND component " +
            "in (\"TEQPM:TEQ\", TEQPM2:TEQ) ORDER BY Rank ASC");
        parser.parseJql("5", "project = FBEK ORDER BY Rank ASC");
        parser.parseJql("6", "project = ITRB AND component = ITRB:Business " +
            "ORDER BY Rank ASC");
    }
}



