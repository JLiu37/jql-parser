public class ParseResult {

	private String boardID = null;
	private String project = null;
	private String component = null;
    private String jql = null;

    public ParseResult(String boardID, String project, String component, String jql) {
        this.boardID = boardID;
        this.jql = jql;
        this.project = project;
        this.component = component;
        log();
    }

    public String getBoardID() {
    	return boardID;
    }

    public String getProject() {
    	return project;
    }

    public String getComponent() {
    	return component;
    }

    public String getJql() {
    	return jql;
    }

    public void log() {
        System.out.println("ParseResult:");
        System.out.println("   boardID = " + boardID);
        System.out.println("   project = " + project);
        System.out.println("   component = " + component);
        System.out.println("   jql = " + jql);
    }

}