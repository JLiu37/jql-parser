import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JQLParser {

    private Scanner scanner;

    public JQLParser(String filename) {
        File f = new File(filename);
        try {
            this.scanner = new Scanner(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public JQLParser() {
    }

    // 
    // source: String to match
    // pattern: String representation of pattern we are searching for
    // index: 
    public String regExMatch(String source, String pattern, int index) {
        // Create a Pattern object
        Pattern r = Pattern.compile(pattern);

        // Now create matcher object.
        Matcher m = r.matcher(source);
        if (m.find()) {
            return m.group(index);
        } else {
            return null;
        }
    }

    public ParseResult parseJql(String boardID, String jql) {
        ParseResult result = null;

        // Look for 'component = "...."'
        String componentClause = regExMatch(jql, "component = \".*\"", 0);
        if (componentClause != null) {
            // Once you have that, isolate the actual value in quotes.
            String value = regExMatch(componentClause, "\".*\"", 0);

            // Remove all leading and trailing spaces so the string starts and ends with double-quotes.
            value = value.trim();

            // Strip out the quotes so all you have is the actual string value inside.
            String quotesStripped = value.substring(1, value.length()-1);

            // Split the string value by ':' to separate the project prefix from the actual component name.
            String[] tokens = quotesStripped.split(":");
            String project = tokens[0];
            String component = tokens[1];

            // return a ParseResult object with everything you need.
            result = new ParseResult(boardID, jql, project, component);
            return result;
        }
        // 'component = "..."' clause not found; move on

        // Look for 'component in ("....","....")'
        componentClause = regExMatch(jql, "component in [(].*[)]", 0);
        if (componentClause != null) {
            // Once you have that, isolate the actual value in (...).
            String valueList = regExMatch(componentClause, "[(].*[)]", 0);

            // Strip out the () so all you have is the actual comma-separated list inside.
            String stripped = valueList.substring(1, valueList.length()-1);

            // Split the string value by ',' to separate the various components into a String array.
            String[] componentList = stripped.split(",");

            // Loop through the list of components now.
            StringBuilder projects = new StringBuilder();
            String component = null;
            for (String s : componentList) {
                // Remove all leading and trailing spaces so the string starts and ends with double-quotes.
                s = s.trim();

                // Strip out the quotes so all you have is the actual string value inside.
                String quotesStripped = s.substring(1, s.length()-1);

                // Split the string value by ':' to separate the project prefix from the actual component name.
                String[] tokens = quotesStripped.split(":");
                String project = tokens[0];
                String temp = tokens[1];

                if (projects.length() == 0) {
                    // Nothing there yet. Just add the first project.
                    projects.append(project);
                    component = temp;
                } else {
                    // Add a comma followed by the project prefix (to create a comma-delimited list).
                    projects.append(",").append(project);
                    // If the components don't all match, the entire thing is invalid.
                    if (!temp.equals(component)) {
                        System.out.println("Current component " + temp + " did not match previous component " + component);
                        result = new ParseResult(boardID, jql, "Invalid", "Unmatched Components");
                        return result;
                    }
                }
            }
            result = new ParseResult(boardID, jql, projects.toString(), component);
            return result;
        }
        // 'component in ("....","....")' clause not found; move on

        // No valid component clauses found
        result = new ParseResult(boardID, jql, "Invalid", "No Components Specified");
        return result;
    }
}

